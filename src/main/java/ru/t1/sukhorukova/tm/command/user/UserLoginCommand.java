package ru.t1.sukhorukova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull private final String NAME = "user-login";
    @NotNull private final String DESCRIPTION = "Sign in.";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");

        System.out.println("Enter login:");
        @Nullable final String login = TerminalUtil.nextLine();

        System.out.println("Enter password:");
        @Nullable final String password = TerminalUtil.nextLine();

        getAuthService().login(login, password);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable @Override
    public Role[] getRoles() {
        return null;
    }

}
