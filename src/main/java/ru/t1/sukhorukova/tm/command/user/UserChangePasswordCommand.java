package ru.t1.sukhorukova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull private final String NAME = "user-change-password";
    @NotNull private final String DESCRIPTION = "Change password.";

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");

        System.out.println("Enter new password:");
        @Nullable final String password = TerminalUtil.nextLine();

        @NotNull final String userId = getAuthService().getUserId();
        getUserService().setPassword(userId, password);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
