package ru.t1.sukhorukova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Show task list by project id.";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");

        System.out.println("Enter project id:");
        @Nullable final String projectId = TerminalUtil.nextLine();

        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(getAuthService().getUserId(), projectId);
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
