package ru.t1.sukhorukova.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ServerVersionResponse extends AbstractResponse {

    @Nullable
    private String version;

}
