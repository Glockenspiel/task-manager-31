package ru.t1.sukhorukova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.dto.request.ServerAboutRequest;
import ru.t1.sukhorukova.tm.dto.request.ServerVersionRequest;
import ru.t1.sukhorukova.tm.dto.response.ServerAboutResponse;
import ru.t1.sukhorukova.tm.dto.response.ServerVersionResponse;

public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final ILocatorService locatorService;

    public SystemEndpoint (@NotNull final ILocatorService locatorService) {
        this.locatorService = locatorService;
    }

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final IPropertyService propertyService = locatorService.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final IPropertyService propertyService = locatorService.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
