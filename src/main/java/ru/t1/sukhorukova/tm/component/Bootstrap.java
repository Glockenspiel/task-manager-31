package ru.t1.sukhorukova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.sukhorukova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.sukhorukova.tm.api.repository.ICommandRepository;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.api.repository.IUserRepository;
import ru.t1.sukhorukova.tm.api.service.*;
import ru.t1.sukhorukova.tm.command.AbstractCommand;
import ru.t1.sukhorukova.tm.dto.request.ServerAboutRequest;
import ru.t1.sukhorukova.tm.dto.request.ServerVersionRequest;
import ru.t1.sukhorukova.tm.endpoint.SystemEndpoint;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.sukhorukova.tm.exception.system.CommandNotSupportedException;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.model.User;
import ru.t1.sukhorukova.tm.repository.CommandRepository;
import ru.t1.sukhorukova.tm.repository.ProjectRepository;
import ru.t1.sukhorukova.tm.repository.TaskRepository;
import ru.t1.sukhorukova.tm.repository.UserRepository;
import ru.t1.sukhorukova.tm.service.*;
import ru.t1.sukhorukova.tm.util.SystemUtil;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements ILocatorService {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.sukhorukova.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes)
            registry(clazz);
    }

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setLocatorService(this);
        commandService.add(command);
    }

    public void start(@NotNull final String[] args) {
        initPID();
        initLogger();
        processArguments(args);
        initDemoData();
        initBackup();
        initFileScanner();
        initServer();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final RuntimeException e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processArguments(@Nullable final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.out.println("[OK]");
            System.exit(0);
        } catch (@NotNull final RuntimeException e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, final boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull User admin = userService.create("admin", "admin", Role.ADMIN);
        @NotNull User user1 = userService.create("USER_01", "user01", "user01@address.ru");
        @NotNull User user2 = userService.create("USER_02", "user02", "user02@address.ru");
        @NotNull User user3 = userService.create("USER_03", "user03", "user03@address.ru");

        projectService.add(new Project(user1, "PROJECT_01", "Test project 1", Status.COMPLETED));
        projectService.add(new Project(user1, "PROJECT_18", "Test project 18", Status.IN_PROGRESS));
        projectService.add(new Project(user2, "PROJECT_02", "Test project 2", Status.NOT_STARTED));
        projectService.add(new Project(user3, "PROJECT_26", "Test project 26", Status.IN_PROGRESS));

        taskService.add(new Task(user1, "TASK_01", "Test task 1", Status.COMPLETED));
        taskService.add(new Task(user1, "TASK_18", "Test task 18", Status.IN_PROGRESS));
        taskService.add(new Task(user1, "TASK_02", "Test task 2", Status.NOT_STARTED));
        taskService.add(new Task(user2, "TASK_26", "Test task 26", Status.IN_PROGRESS));
    }

    private void initBackup() {
        backup.init();
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    private void initServer() {
        server.start();
    }

}
